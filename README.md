This is the data repository for the results and trained models of the publication https://doi.org/10.1186/s42162-021-00181-5.

The code that was used to generate the data can be found here: https://gitlab.com/digitalized-energy-systems/scenarios/reactive_market_attack

The README there also contains information how to reproduce the exact results and figures from the publication.